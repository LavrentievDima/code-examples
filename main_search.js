'use strict';

let brands_clean_list_glob = [];
let start_time;
let filter_price_min = 0;
let filter_price_max = 10000000000;
let search_mode = 'search_str';     // article_brand
let last_clicked_art_brand_search__brand_clean = null;

let delivery_time_glob = [];
let delivery_from_glob = null;
let delivery_to_glob = null;

let search_in_progress = false;


let calculateWidthBrandCards = () => {
    let carusele_items = $('#panel_brands .carousel-item');
    if (carusele_items.length > 1) {
        let width = carusele_items.find('.search-by-article-brand').first().width();
        carusele_items.each((i, el) => {
            if ($(el).find('.search-by-article-brand').length < 5) {
                $(el).find('.search-by-article-brand').each((j, el_brand) => {
                    $(el_brand).removeClass('col');
                    $(el_brand).width(width);
                });
            }
        });
    }
};

// $(document).ajaxError(function (event, jqxhr, settings, thrownError) {
//     alert_my('Ошибка аякс', '' + jqxhr.status + ' ' + thrownError);
// });

$(document).ready(function () {


    $('.search_mode_other a').click(function (e) {
        e.preventDefault();
        $('.search_mode_other a').removeClass('active');
        $(this).addClass('active');
    });

    let currentCustomerChange = () => {

        let params = url_get_params();

        if (search_mode === 'search_str') {
            search(
                params.q,
                params.currency,
                filter_price_min,
                filter_price_max,
                brands_clean_list_glob,
                delivery_time_glob
            );
        } else {
            last_clicked_art_brand_search__brand_clean = null;
            search_by_article_brand(
                params.article,
                params.brand,
                params.currency,
                filter_price_min,
                filter_price_max,
                brands_clean_list_glob,
                delivery_time_glob,
                null
            );
        }
    };

    $(document).on('click', '#search_button', function () {
        user_parameters.refresh_params('get').then(() => {

            $('.search-suppliers-table-header').addClass('d-none');
            $('.search-stock-table-header').addClass('d-none');
            $('[data-toggle="popover"]').popover('dispose');

            search($('#search_str').val(), getCurrencyId());

            $('#option-view-list').click();

            delivery_from_glob = null;
            delivery_to_glob = null;


        });
    });


    let params = url_get_params();

    if (
        params.q !== null &&
        params.currency !== null
    ) {
        user_parameters.refresh_params({
            sell_currency_id: params.currency
        }).then(() => {
            search(
                params.q,
                user_parameters.sell_currency_id,
                filter_price_min,
                filter_price_max,
                [],
                [],
                params.product_group_id
            );
        });
    } else if (
        params.article !== null &&
        params.brand !== null &&
        params.currency !== null
    ) {
        user_parameters.refresh_params({
            sell_currency_id: params.currency
        }).then(() => {
            $('#search_str').val(params.article);

            checkRemoveTextBtn();

            search_by_article_brand(
                params.article,
                params.brand,
                user_parameters.sell_currency_id,
                filter_price_min,
                filter_price_max,
                [],
                [],
                params.product_group_id
            );
        });
    } else {
        alert_my('', 'Тип поиска не определен');
    }


    //слушатель события изменения валюты currency_radio
    $('.btn-group-currency').find('input:radio').change(function () {
        user_parameters.refresh_params({
            sell_currency_id: $(this).val()
        }).then(() => {

            params = url_get_params();

            let str_search = $('#search_str').val();
            if (str_search === '') {
                str_search = params.q;
            }

            if (search_mode === 'search_str') {
                search(
                    str_search,
                    user_parameters.sell_currency_id,
                    filter_price_min,
                    filter_price_max,
                    brands_clean_list_glob
                );

            } else {
                last_clicked_art_brand_search__brand_clean = null;

                search_by_article_brand(
                    params.article,
                    params.brand,
                    user_parameters.sell_currency_id,
                    filter_price_min,
                    filter_price_max,
                    brands_clean_list_glob,
                    [],
                    null
                );
            }

        });
    });


    $('#current_customer').change(function () {
        user_parameters.refresh_params({}).then(() => {
            currentCustomerChange();
        });

    });

    $('.carousel').carousel();

    $(document).on('click', '.btn-decrement', function (e) {

        let input = $(e.target).closest('.numberator_wrapper').find('input');
        if (parseInt(input.val()) > 0) {
            input.val(parseInt(input.val()) - 1);
        }
        initModalCounterBtnBody(this);
    });

    $(document).on('click', '.btn-increment', function (e) {
        let input = $(e.target).closest('.numberator_wrapper').find('input');
        // -- Проверка количества товара при добавлении в корзину---------- раскоментировать, если нужно----------
        let max_quantity = $(e.target).closest('.row.warehouse-item').data('quantity');

        max_quantity = parseInt(max_quantity.toString());
        if (isNaN(max_quantity)) {
            max_quantity = 999;
        }
        // console.log('input.val(+)', input.val(), max_quantity);
        if (parseInt(input.val()) < max_quantity) {
            input.val(parseInt(input.val()) + 1);
            initModalCounterBtnBody($(e.target));
        } else {
            // alert('По данному складу не достаточно товара');
            myOwnAlert({
                mode: 'alert',
                content: 'По данному складу не достаточно товара.'
            });
        }

    });

    $(document).on('keyup', '.shopping-cart-quantity', function (e) {
        if (e.which == 44 || e.which == 45 || e.which == 43 || e.which == 46 || e.which == 47) return false;
    });

    $(document).on('keyup', '.shopping-cart-quantity', function (e) {
        let input = $(e.target);
        let max_quantity = $(e.target).closest('.warehouse-item').data('quantity');
        max_quantity = parseInt(max_quantity.toString());
        if (isNaN(max_quantity)) {
            max_quantity = 999;
        }
        if (input.val() === '') input.val(0);
        // console.log('input.val( )', input.val(), max_quantity);

        if (parseInt(input.val()) <= max_quantity) {
            initModalCounterBtnBody($(e.target));
        } else if (parseInt(input.val()) > max_quantity) {
            myOwnAlert({
                mode: 'alert',
                content: 'По данному складу не достаточно товара.'
            });
            input.val(max_quantity);
        }
    });


    $('.change_view_button .btn').click(function () {

        $('.change_view_button .btn').removeClass('active_btn');
        $(this).addClass('active_btn');

        let searchResultTypeView = $(this).data('type-view');

        $('.search_result_container').attr('data-result-view-type', searchResultTypeView);
        localStorage.setItem('type_view_product', $(this).data('type-view'));

    });

    $(document).ready(function () {

        let typeView = localStorage.getItem('type_view_product') ? localStorage.getItem('type_view_product') : 'standart';
        $('.btn-invert').each(function (i, el) {
            if ($(el).attr('data-type-view') === typeView && typeView) {
                $(el).addClass('active_btn');
                // localStorage.removeItem('type_view_product') ;
            } else if (typeView) {
                $(el).removeClass('active_btn');
            }
            $('.search_result_container').attr('data-result-view-type', typeView);
        });

    });

    $(document).on('click', '.link_search_brand_articl', (e) => {

        e.preventDefault();

        const href = $(e.target).closest('.link_search_brand_articl').attr('href');

        const brand_tmp = href.split('brand=')[1];
        const brand = brand_tmp.slice(0, brand_tmp.indexOf('&'));

        const article_tmp = href.split('article=')[1];
        const article = article_tmp.slice(0, article_tmp.indexOf('&'));

        search_by_article_brand(
            article,
            brand,
            user_parameters.sell_currency_id,
            filter_price_min,
            filter_price_max,
            [],
            [],
            params.product_group_id
        );
    });

});


function show_left_panel(
    price_min = filter_price_min,
    price_max = filter_price_max,
    brands_clean_list = [],
    delivery_times = [],
    query_id = null
) {
    let currency_id = getCurrencyId();
    let search_str_req = $('#search_str').val();

    return $.ajax({
        url: '/api/search_panel/',
        type: 'post',
        dataType: 'json',
        headers: {
            'X-CSRFToken': $('#csrf_token').find('input[name=csrfmiddlewaretoken]').val(),
            'Content-Type': 'application/json'
        },
        data: JSON.stringify({
            price_min: price_min,
            price_max: price_max,
            brands_clean_list: brands_clean_list,
            delivery_times: delivery_times,
            sell_currency_id: currency_id,
            search_str_req: search_str_req,
            query_id: query_id
        }),
        success: function (data) {
            let html = base64.decode(data.html_base64);
            html = utf8.decode(html);

            const {brand_panel_list, html_brands_panel, row_count} = data;

            $('#panel .preload_ui').addClass('d-none');
            $('#panel .loaded_ui').html(html);

            if (brand_panel_list.length > 0) {
                $('#panel_brands .preload_ui').addClass('d-none');
                $('#panel_brands .loaded_ui').html(html_brands_panel);
            } else {
                $('#panel_brands .preload_ui').addClass('d-none');
            }
            $('#search_count').html(
                `
                    <span class="search_all_count">
                        Всего найдено:  ${row_count}  (${((performance.now() - start_time) / 1000).toFixed(2)} s)
                    </span>
                `
            );
            if (row_count === 0) {
                $('#panel_brands .preload_ui').addClass('d-none');
                $('#panel_brands .loaded_ui').empty();
                $('#message-block').html('<h1 class="mt-3">Ничего не найдено</h1>');
            }
            let params = url_get_params();

            $('.filter_by_price').click(function () {
                filter_price_max = parseInt($(this).data('price-max').toString());
                filter_price_min = parseInt($(this).data('price-min').toString());

                let str_search = $('#search_str').val();
                if (str_search === '') {
                    str_search = params.q;
                }

                if (search_mode === 'search_str') {
                    search(
                        str_search,
                        currency_id,
                        filter_price_min,
                        filter_price_max,
                        brands_clean_list,
                        delivery_times
                    );
                } else {
                    search_by_article_brand(
                        params.article,
                        params.brand,
                        currency_id,
                        filter_price_min,
                        filter_price_max,
                        brands_clean_list_glob,
                        []
                    );
                }
            });

            $('input[name=select_brand]').click(function () {
                brands_clean_list_glob = [];

                const count_checed_bend = $('input[name=select_brand]').filter(':checked').length;

                if (count_checed_bend === 0) {
                    $('#id_select_brand_0').prop('checked', true);
                }
                // if(count_checed_bend === 1){
                //     $('#id_select_brand_0').prop('checked', true);
                // }
                if (count_checed_bend > 1) {
                    if ($(this).attr('id') === 'id_select_brand_0') {
                        $('input[name=select_brand]').prop('checked', false);
                        $('#id_select_brand_0').prop('checked', true);
                    } else {
                        $('#id_select_brand_0').prop('checked', false);
                    }

                }

                $('input[name=select_brand]').filter(':checked').each(function (index, item) {
                    if ($(this).val() !== '') {
                        brands_clean_list_glob.push($(this).val());
                    }
                });

                if ($(this).prop('checked') === false) {
                    brands_clean_list_glob = brands_clean_list_glob.filter(item => item !== $(this).val());
                }

                if ($(this).val() === '') brands_clean_list_glob = [];

                let str_search = $('#search_str').val();
                if (str_search === '') {
                    str_search = params.q;
                }

                if (search_mode === 'search_str') {
                    search(
                        str_search,
                        currency_id,
                        filter_price_min,
                        filter_price_max,
                        brands_clean_list_glob,
                        delivery_time_glob
                    );
                } else {
                    last_clicked_art_brand_search__brand_clean = null;

                    search_by_article_brand(
                        params.article,
                        params.brand,
                        currency_id,
                        filter_price_min,
                        filter_price_max,
                        brands_clean_list_glob,
                        delivery_time_glob
                    );
                }
            });

            $('input[name=select_delivery_time]').click(function () {
                delivery_time_glob = [];

                $('input[name=select_delivery_time]').filter(':checked').each(function (index, item) {
                    delivery_time_glob.push($(this).val());
                });

                if ($(this).prop('checked') === false) {
                    delivery_time_glob = delivery_time_glob.filter(item => item !== $(this).val());
                }

                if ($(this).val() === '') delivery_time_glob = [];

                let str_search = $('#search_str').val();
                if (str_search === '') {
                    str_search = params.q;
                }

                if (search_mode === 'search_str') {
                    search(
                        str_search,
                        currency_id,
                        filter_price_min,
                        filter_price_max,
                        brands_clean_list_glob,
                        delivery_time_glob
                    );
                } else {
                    last_clicked_art_brand_search__brand_clean = null;

                    search_by_article_brand(
                        params.article,
                        params.brand,
                        currency_id,
                        filter_price_min,
                        filter_price_max,
                        brands_clean_list_glob,
                        delivery_time_glob
                    );
                }
            });

            $('.search-by-article-brand').off('click').click(function () {
                $('[data-toggle="popover"]').popover('dispose');

                delivery_from_glob = null;
                delivery_to_glob = null;

                search_by_article_brand(
                    $(this).data('article-clean'),
                    $(this).data('brand-clean'),
                    getCurrencyId())
                ;
            });

            shopping_cart_init();

            $('[data-toggle="popover"]').popover();

            $('.for-table-collapse ').each(function (i, el) {
                if ($(el).find('.row-to-collapse').length !== 0) {
                    $(el).find('.expand-collapse-btn-div').removeClass('d-none');
                    $(el).find('.expand-collapse-btn-div .expand-collapse-showe-btn').removeClass('d-none');
                    $(el).closest('.price_row.row').addClass('row_with_collapse');
                }
            });

            let collapseAllRows = () => {
                $('.for-table-collapse').each((i, el) => {
                    $(el).find('.expand-collapse-hide-btn').addClass('d-none');
                    $(el).find('.expand-collapse-showe-btn').removeClass('d-none');
                    $(el).find('.row-to-collapse').addClass('d-none');
                    $(el).css({boxShadow: 'none', position: 'absolute', top: '100%'});
                    $(el).removeClass('long-shadow');
                });
            };
            $('.expand-collapse-showe-btn').on('click', function () {
                collapseAllRows();
                $(this).addClass('d-none');
                $(this).siblings().removeClass('d-none');
                $(this).closest('.for-table-collapse').find('.row-to-collapse').removeClass('d-none');
                $(this).closest('.for-table-collapse').css({position: 'relative', top: '0'});
            });
            $('.expand-collapse-hide-btn').on('click', function () {
                collapseAllRows();
                $(this).siblings().removeClass('d-none');
            });

            $(document).on('click', '.show-price-row-data-not-in-stock', function () {

                show_admin_price_row_data(
                    $(this).closest('.price_row').data('uuid'),
                    user_parameters.customer_id,
                    user_parameters.sell_currency_id
                );
            });

            $(document).on('click', '.show-price-row-data-stock', function () {
                show_admin_price_row_data(
                    $(this).closest('.price-row-warehouse').data('price-row-uuid'),
                    user_parameters.customer_id,
                    user_parameters.sell_currency_id
                );
            });

            let delivery_time_range_data = null;
            let delivery_times_data = null;
            let slider_input = $('.slider-input');
            if (slider_input.length !== 0) {
                delivery_time_range_data = slider_input.data('delivery_time_range');
                delivery_times_data = slider_input.data('delivery_times');

                if (delivery_times_data.length === 0) {
                    slider_input.val(`${getMinInRange(delivery_time_range_data)},${getMaxInRange(delivery_time_range_data)}`);
                } else {
                    slider_input.val(`${getMinInRangeCast(delivery_times_data)},${getMaxInRangeCast(delivery_times_data)}`);
                }
            }
            let scale = getScale(delivery_time_range_data);

            let str_search = $('#search_str').val();
            if (str_search === '') {
                str_search = params.q;
            }

            let searchByDelivery = (delivery_from, delivery_to) => {

                // let list = getRangeFromTo(parseInt(val.split(',')[0]), parseInt(val.split(',')[1]));

                // list = list.join('.').split('.');

                delivery_from_glob = parseInt(delivery_from);
                delivery_to_glob = parseInt(delivery_to);

                if (search_mode === 'search_str') {
                    search(
                        str_search,
                        currency_id,
                        filter_price_min,
                        filter_price_max,
                        brands_clean_list_glob,
                        [],
                        null,
                        delivery_from,
                        delivery_to
                    );
                } else {
                    last_clicked_art_brand_search__brand_clean = null;

                    search_by_article_brand(
                        params.article,
                        params.brand,
                        currency_id,
                        filter_price_min,
                        filter_price_max,
                        brands_clean_list_glob,
                        [],
                        null,
                        delivery_from,
                        delivery_to
                    );
                }
            };

            slider_input.jRange({
                from: getMinInRange(delivery_time_range_data),
                to: getMaxInRange(delivery_time_range_data),
                step: 1,
                scale: getScaleFrom0ToMax(delivery_time_range_data),
                format: '%s',
                width: 170,
                showLabels: true,
                isRange: true,
                snap: true,
                ondragend: function (value) {
                    let splitted = value.split(',');

                    searchByDelivery(splitted[0], splitted[1]);
                },
                onbarclicked: function (value) {
                    let splitted = value.split(',');

                    searchByDelivery(splitted[0], splitted[1]);
                }
            });

            if (Number.isInteger(delivery_from_glob) && Number.isInteger(delivery_to_glob)) {
                slider_input.jRange('setValue', `${delivery_from_glob},${delivery_to_glob}`);
            }

            $('.slider-container ins').each((i, el) => {
                $(el).addClass('label-secondary');
                scale.forEach((elem, i, arr) => {
                    if ($(el).text() === elem.toString()) {
                        $(el).removeClass('label-secondary');
                    } else {
                        $(el).addClass('label-important');
                    }
                });
            });

        },
        complete: function () {

            search_in_progress = false;

            hideProgresBar();
            $('#panel_brands .progress_brand').addClass('d-none');
            // $('.progress').addClass('d-none');
        }
    });
}

function search(
    search_str,
    currency_id,
    price_min = filter_price_min,
    price_max = filter_price_max,
    brands_clean_list = [],
    delivery_times = [],
    product_group_id = null,
    delivery_from = null,
    delivery_to = null
) {

    if (search_in_progress) {
        return;
    } else {
        search_in_progress = true;
    }

    if (only_letters_and_digits(search_str).length < 3) {
        alert_my('Внимание!', 'Для поиска введите 3 или более символов, считая буквы и цифры');
        search_in_progress = false;
        return false;
    }

    $('#panel_brands .loaded_ui').empty();
    $('#panel_brands .preload_ui').removeClass('d-none');

    if ($('#message-block h1').text().indexOf('Ничего не найдено') !== -1) {
        $('.result-search-block .loaded_ui').empty();
        $('.result-search-block .preload_ui').removeClass('d-none');

        $('#panel .loaded_ui').empty();
        $('#panel .preload_ui').removeClass('d-none');
    }

    last_clicked_art_brand_search__brand_clean = null;
    start_time = performance.now();
    search_mode = 'search_str';

    let customer_id = user_parameters.customer_id;
    let query_id = uuidv1();

    $('#search_str').val(search_str.trim());

    checkRemoveTextBtn();

    history.replaceState(
        {q: search_str},
        null,
        create_get_params([
            {q: search_str},
            {currency: currency_id},
            {product_group_id: product_group_id}
        ])
    );

    document.title = `Поиск ${search_str} ${$('#project_data').data('company_name')} `;

    $('.result-search-block .progress_search_result').removeClass('d-none');

    $('#panel_brands .progress_brand').removeClass('d-none');

    blokUI();

    if (customer_id === null) {
        alert_my('Ошибка', 'Внимание, не сопоставлен покупатель пользователю');
        return false;
    }

    let data = {
        search_str: search_str,

        price_min: price_min,
        price_max: price_max,

        brands_clean_list: brands_clean_list,
        delivery_times: delivery_times,
        customer_id: customer_id,
        sell_currency_id: currency_id,
        search_mode_other: $('.search_mode_other .active').attr('data-search-mode'),
        product_group_id: product_group_id,
        result_mode: 'main_warehouse',

        delivery_from: delivery_from,
        delivery_to: delivery_to,

        query_id: query_id

    };

    removeAutoload();

    $('#message-block').empty();

    $.when(
        showProgresBar(),
        sendRequestSearch('/api/search_by_article/', data, 'main_warehouse', '#articles_warehouse'),
        sendRequestSearch('/api/search_by_article/', data, 'is_not_main_warehouse', '#articles_not_warehouse'),
        sendRequestSearch('/api/search_other/', data, 'main_warehouse', '#other_warehouse'),
        sendRequestSearch('/api/search_other/', data, 'is_not_main_warehouse', '#other_not_warehouse'),
        sendRequestSearch('/api/search_by_cross/', data, 'main_warehouse', '#cross_warehouse'),
        sendRequestSearch('/api/search_by_cross/', data, 'is_not_main_warehouse', '#cross_not_warehouse')
    ).then(function (data, textStatus, jqXHR) {
        $.when(
            $.ajax({
                url: '/api/search_by_supplier_api/',
                type: 'post',
                dataType: 'json',
                headers: {
                    'X-CSRFToken': $('#csrf_token').find('input[name=csrfmiddlewaretoken]').val(),
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify({
                    search_str: search_str,
                    price_min: price_min,
                    price_max: price_max,
                    brands_clean_list: brands_clean_list,
                    delivery_times: delivery_times,
                    customer_id: customer_id,
                    sell_currency_id: currency_id,
                    search_mode_other: $('.search_mode_other .active').attr('data-search-mode'),
                    product_group_id: product_group_id,
                    result_mode: 'is_not_main_warehouse'
                }),
                success: function (data) {

                    const {articles, cross} = data;

                    if (articles.html !== '') {
                        const statistic = $('#articles_not_warehouse .search_statistic');
                        let massage_time = statistic.text();
                        $('#articles_not_warehouse').html(articles.html);
                        statistic.text(massage_time + ' АПИ ' + getSearchInfoTime(articles, start_time));
                    }

                    if (articles.html.length > 50) {
                        showTableHeader('no_stock');
                    }
                    if (cross.html !== '') {
                        const statistic = $('#cross_not_warehouse .search_statistic');
                        let massage_time = statistic.text();
                        $('#cross_not_warehouse').html(cross.html);
                        statistic.text(massage_time + ' АПИ ' + getSearchInfoTime(cross, start_time));
                    }

                    if (cross.html.length > 50) {
                        showTableHeader('no_stock');
                    }

                    hideTableHeader();
                }
            })
        ).then(function (data, textStatus, jqXHR) {

            $('.custom_scroll_container').animate({scrollTop: 0}, 0);

            if (brands_clean_list.length === 0) {
                show_left_panel(
                    price_min,
                    price_max,
                    brands_clean_list,
                    delivery_times,
                    query_id
                ).then(function () {
                    showHideButtonControle();
                    calculateWidthBrandCards();
                    try {
                        customScroll();
                    } catch (e) {
                        console.log(e);
                    }

                });
            } else {
                search_in_progress = false;
            }
            // $('main').unblock();
            unblokUI();
            initNumberator();

        }).catch(function (data, textStatus, jqXHR) {
            console.log(data, textStatus, jqXHR);
        });

    }).catch(function (data, textStatus, jqXHR) {
        console.log(data, textStatus, jqXHR);

        //console.log(data, textStatus, jqXHR);

        // $('main').unblock();
        unblokUI();
        // $.unblockUI();

        initNumberator();

    });
}

function search_by_article_brand(
    article,
    brand,
    currency_id,
    price_min = filter_price_min,
    price_max = filter_price_max,
    brands_clean_list = [],
    delivery_times = [],
    product_group_id = null,
    delivery_from = null,
    delivery_to = null
) {
    if (search_in_progress) {
        return;
    } else {
        search_in_progress = true;
    }

    $('#panel_brands .loaded_ui').empty();
    if ($('#message-block h1').text().indexOf('Ничего не найдено') !== -1) {
        $('.result-search-block .loaded_ui').empty();
        $('.result-search-block .preload_ui').removeClass('d-none');

        $('#panel .loaded_ui').empty();
        $('#panel .preload_ui').removeClass('d-none');
    }

    blokUI();

    let customer_id = user_parameters.customer_id;
    let query_id = uuidv1();

    start_time = performance.now();

    search_mode = 'article_brand';

    $('.result-search-block .progress_search_result').removeClass('d-none');

    $('#panel_brands .progress_brand').removeClass('d-none');

    history.replaceState(
        {
            article: article,
            brand: brand
        },
        `Поиск ${article} - ${brand}`,
        create_get_params([
            {article: article},
            {brand: brand},
            {currency: currency_id},
            {product_group_id: product_group_id}
        ])
    );

    document.title = `Поиск ${article} - ${brand} ${$('#project_data').data('company_name')} `;

    if (last_clicked_art_brand_search__brand_clean === only_letters_and_digits(brand)) {
        search_in_progress = false;

        $('#search_button').click();
        $('#option-view-list').click();

        last_clicked_art_brand_search__brand_clean = null;

        return false;
    }

    last_clicked_art_brand_search__brand_clean = only_letters_and_digits(brand);

    let data = {
        article: article,
        brand: brand,
        price_min: price_min,
        price_max: price_max,
        brands_clean_list: brands_clean_list,
        delivery_times: delivery_times,
        customer_id: customer_id,
        sell_currency_id: currency_id,
        search_mode_other: $('.search_mode_other .active').attr('data-search-mode'),
        product_group_id: product_group_id,
        result_mode: 'main_warehouse',
        delivery_from: delivery_from,
        delivery_to: delivery_to,

        query_id: query_id
    };

    $('#message-block').empty();

    $.when(
        sendRequestSearchByArticleBrand('/api/search-article-by-article-brand/', data, 'main_warehouse', '#articles_warehouse'),
        sendRequestSearchByArticleBrand('/api/search-article-by-article-brand/', data, 'is_not_main_warehouse', '#articles_not_warehouse'),
        sendRequestSearchByArticleBrand('/api/search-cross-by-article-brand/', data, 'main_warehouse', '#cross_warehouse'),
        sendRequestSearchByArticleBrand('/api/search-cross-by-article-brand/', data, 'is_not_main_warehouse', '#cross_not_warehouse'),
    ).then(function (data, textStatus, jqXHR) {

        $('.custom_scroll_container').animate({scrollTop: 0}, 0);

        $('.result-search-block  .progress_search_result').addClass('d-none');
        $('.result-search-block .preload_ui').addClass('d-none');
        if (brands_clean_list.length === 0) {
            show_left_panel(price_min, price_max, brands_clean_list, delivery_times)
                .then(function () {
                    $('.search-by-article-brand').removeClass('active');
                    $('.search-by-article-brand').filter(`[data-brand-clean="${only_letters_and_digits(brand)}"]`).addClass('active');
                    showHideButtonControle();
                    calculateWidthBrandCards();
                    // $('main').unblock();
                    unblokUI();

                    try {
                        customScroll();
                    } catch (e) {
                        console.log(e);
                    }
                });
        } else {
            $('.search-by-article-brand').removeClass('active');
            $('.search-by-article-brand').filter(`[data-brand-clean="${only_letters_and_digits(brand)}"]`).addClass('active');
            showHideButtonControle();
            calculateWidthBrandCards();
            // $('main').unblock();
            unblokUI();

            try {
                customScroll();
            } catch (e) {
                console.log(e);
            }
            search_in_progress = false;
        }

        initNumberator();

        $('#search_str').val(article);

    });
}


// //-- Stick table head on scroll-----------------------------------------------------
//
// function initHeaderStockTable() {
//     let html = $('.stock-table-head-hiden .block-to-copy-by-header-table-stock').html();
//     $('.search-stock-table-header .block-to-pass-by-header-table-stock').html(html);
// }
//
// function showTableHeader(type) {
//     if (type === 'stock') {
//         $('.search-stock-table-header').removeClass('d-none');
//     } else {
//         $('.search-suppliers-table-header').removeClass('d-none');
//     }
// }
//
// function hideTableHeader() {
//     if ($('.search-stock-table-header').length > 0) {
//         $('.search-stock-table-header').each(function (i, el) {
//             if (i > 0) {
//                 $(el).addClass('d-none');
//             } else {
//                 $(el).addClass('d-none d-md-block');
//             }
//         });
//     }
//     if ($('.search-suppliers-table-header').length > 0) {
//         $('.search-suppliers-table-header').each(function (i, el) {
//             if (i > 0) {
//                 $(el).addClass('d-none');
//             } else {
//                 $(el).addClass('d-none d-md-block');
//             }
//         });
//     }
// }

function getSearchInfoTime(data, start_time) {
    const {row_count} = data;
    return `Найдено: ${row_count} (${((performance.now() - start_time) / 1000).toFixed(2)} s)`;
}


function refactorFormatNumberInSearch(table) {
    table.find('button b').each(function (i, el) {
        $(el).text(refactorFormatNumber($(el).data('value-price')));
    });
}

function showHideButtonControle() {
    let controlButtons = $('#panel_brands .carousel-control-prev, #panel_brands .carousel-control-next');
    if ($('#panel_brands .carousel-item').length === 1) {
        controlButtons.addClass('d-none');
    } else {
        controlButtons.removeClass('d-none');
    }
}


function sendRequestSearch(url, data, type_warehouse, html_block_id) {

    data['result_mode'] = type_warehouse;

    data = JSON.stringify(data);

    let start_time = performance.now();
    let block = $(html_block_id);

    return $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        headers: {
            'X-CSRFToken': $('#csrf_token').find('input[name=csrfmiddlewaretoken]').val(),
            'Content-Type': 'application/json'
        },
        data: data,
        success: function (data) {
            block.find('.preload_ui').addClass('d-none');
            block.find('.loaded_ui').html(data.html);

            block.find('.search_statistic').text(
                getSearchInfoTime(data, start_time)
            );

            initHeaderStockTable();
            if (data.html.length > 50) {
                showTableHeader(type_warehouse);
            }
            hideTableHeader();
            initModalGoodsCounterInBlock($(html_block_id));

            block.find('.progress_search_result').addClass('d-none');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.responseText.indexOf('Ensure this field has no more than 255 characters')) {
                $('.progress_main').addClass('d-none');
                $('#panel_brands .preload_ui').addClass('d-none');
                $('#panel_brands .progress_brand').addClass('d-none');
                block.find('.preload_ui').addClass('d-none');
                block.find('.progress_search_result').addClass('d-none');
                // $('main').html('<h1 class="mt-3"></h1>');
                // alert(`Ошибка поиска в блоке с id = ${html_block_id}`);
                myOwnAlert({
                    mode: 'alert',
                    content: 'Количество введенных символов превышает допустимое.'
                });
            }
        },
        complete: function () {
            delivery_time_glob = [];
        }
    });
}

function sendRequestSearchByArticleBrand(url, data, type_warehouse, html_block_id) {

    data['result_mode'] = type_warehouse;
    data = JSON.stringify(data);
    let start_time = performance.now();
    let block = $(html_block_id);

    return $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        headers: {
            'X-CSRFToken': $('#csrf_token').find('input[name=csrfmiddlewaretoken]').val(),
            'Content-Type': 'application/json'
        },
        data: data,
        success: function (data) {
            block.find('.loaded_ui').html(data.html);
            block.find('.preload_ui').addClass('d-none');
            block.find('.progress_search_result').addClass('d-none');
            block.find('.search_statistic').text(getSearchInfoTime(data, start_time));
            initHeaderStockTable();
            if (data.html.length > 50) {
                showTableHeader('stock');
            }
            hideTableHeader();
        },
    });
}




