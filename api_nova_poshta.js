const apiKeyNP = '';

let getCityesNP = () => {
    let params = {
        'apiKey': apiKeyNP,
        'modelName': 'Address',
        'calledMethod': 'getCities',
        'methodProperties': {
            'Language': 'ru',
        }
    };
    $.ajax({
        url: 'https://api.novaposhta.ua/v2.0/json/?' + $.param(params),
        beforeSend: function (xhrObj) {
            xhrObj.setRequestHeader('Content-Type', 'application/json');
            return Number;
        },
        type: 'POST',
        dataType: 'jsonp',
        data: '{body}',
        success: function ({data}) {
            // console.log(data);
            let html = '<option value="">Выберите город</option>';
            data.forEach((el) => {
                html += `<option value="${el.Ref}">${el.DescriptionRu}</option>`;
            });
            $('.select-city').html(html);
            $('.select-city').select2({width: 'resolve',containerCssClass: 'ownStyle_delivery_warehouse', dropdownCssClass: 'ownStyle_delivery_warehouse'});
        },
    });
};

let getWarehousesNP = (CityRef) => {
    let params = {
        'apiKey': apiKeyNP,
        'modelName': 'AddressGeneral',
        'calledMethod': 'getWarehouses',
        'methodProperties': {
            'Language': 'ru',
            'CityRef': CityRef
        }
    };
    $.ajax({
        url: 'https://api.novaposhta.ua/v2.0/json/?' + $.param(params),
        beforeSend: function (xhrObj) {
            xhrObj.setRequestHeader('Content-Type', 'application/json');
            return Number;
        },
        type: 'POST',
        dataType: 'jsonp',
        data: '{body}',
        success: function ({data}) {
            let html = '<option value="">Выберите склад</option>';
            data.forEach((el) => {
                html += `<option value="${el.SiteKey}">${el.DescriptionRu}</option>`;
            });
            $('.select-warehouse').html(html);
            $('.select-warehouse').select2({width: 'resolve', containerCssClass: 'ownStyle_delivery_warehouse', dropdownCssClass: 'ownStyle_delivery_warehouse'});
        },
    });
};







