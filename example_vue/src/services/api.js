import axios from 'axios'
import { appConfig } from '@/utils/config'

const { domain } = appConfig
const api = axios.create({
  baseURL: domain + 'wp-json/betslip/v1/betslip/',
})

export default api
