/* eslint-disable no-useless-escape */

function getParameterByUrl(name, url) {
  if (!url) url = window.location.href
  name = name.replace(/[\[\]]/g, '\\$&')
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)')
  var results = regex.exec(url)
  if (!results) return null
  if (!results[2]) return ''
  return decodeURIComponent(results[2].replace(/\+/g, ' '))
}

function isEmpty(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) return false
  }
  return true
}

function toCamelCase(str) {
  str = str.toLowerCase().replace(/(?:(^.)|([-_\s]+.))/g, function(match) {
    return match.charAt(match.length - 1).toUpperCase()
  })
  return str.charAt(0).toLowerCase() + str.substring(1)
}

function toPascalCase(str) {
  if (!str) return ''
  return String(str)
    .replace(/^[^A-Za-z0-9]*|[^A-Za-z0-9]*$/g, '$')
    .replace(/[^A-Za-z0-9]+/g, '$')
    .replace(/([a-z])([A-Z])/g, (m, a, b) => a + '$' + b)
    .toLowerCase()
    .replace(/(\$)(\w?)/g, (m, a, b) => b.toUpperCase())
}

function groupBy(xs, f) {
  // eslint-disable-next-line no-sequences
  return xs.reduce((r, v, i, a, k = f(v)) => ((r[k] || (r[k] = [])).push(v), r), {})
}

function unique(value, index, self) {
  return self.indexOf(value) === index
}

function getCombinations(set, k) {
  var i, j, combs, head, tailcombs
  if (k > set.length || k <= 0) {
    return []
  }
  if (k === set.length) {
    return [set]
  }
  if (k === 1) {
    combs = []
    for (i = 0; i < set.length; i++) {
      combs.push([set[i]])
    }
    return combs
  }

  combs = []
  for (i = 0; i < set.length - k + 1; i++) {
    head = set.slice(i, i + 1)
    tailcombs = getCombinations(set.slice(i + 1), k - 1)
    for (j = 0; j < tailcombs.length; j++) {
      combs.push(head.concat(tailcombs[j]))
    }
  }
  const filteredCombsUnique = combs.filter(comb => {
    const ids = comb.map(item => item.eventId)
    return isDuplicates(ids)
  })
  return filteredCombsUnique
}

function isDuplicates(array) {
  return new Set(array).size === array.length
}

export { getParameterByUrl, isEmpty, toCamelCase, groupBy, toPascalCase, unique, getCombinations, isDuplicates }
