import Selection from '@/components/common/Selection'
import { mapGetters } from 'vuex'
export default {
  data: () => ({
    limited: true,
  }),
  components: { Selection },
  computed: {
    ...mapGetters(['isActiveBet']),
    odds(market) {
      return market => {
        return this.limited && this.limit ? market.odds.slice(0, this.limit) : market.odds
      }
    },
    limitedText() {
      return this.limited ? 'Show More' : 'Show Less'
    },
  },
  props: {
    markets: {
      type: Array,
      required: false,
    },
    market: {
      type: Object,
      required: false,
    },
    limit: {
      type: Number,
      required: false,
    },
  },
  methods: {
    toggleLimitOdds() {
      this.limited = !this.limited
    },
  },
}
