import Vue from 'vue'

import '@/assets/scss/styles.scss'
import '@/plugins/date'
import '@/plugins/currency'
import '@/plugins/mq'
import '@/plugins/collapse'

import Betslip from '@/components/betslip/Betslip'
import Events from '@/components/events/Events'
import SeparateOdds from '@/components/separate-odds/SeparateOdds'
import createStore from '@/store/index'
import { initialState } from '@/utils/config'
// eslint-disable-next-line no-undef
initialState.appConfig.version = BS_VERSION

const eventsEl = document.getElementsByClassName('js-bs-events')
const separateOdd = document.getElementsByClassName('js-bs-separate')
const betslipEl = document.getElementById('js-bs-controller')
const store = createStore(initialState)

Vue.config.productionTip = false
Vue.config.performance = process.env.NODE_ENV !== 'production'

if (eventsEl) {
  Array.from(eventsEl).forEach(el => {
    new Vue({
      store,
      render: function(h) {
        const context = {
          props: { ...el.dataset },
        }
        return h(Events, context)
      },
    }).$mount(el)
  })
}

if (separateOdd) {
  Array.from(separateOdd).forEach(el => {
    new Vue({
      store,
      render: function(h) {
        const context = {
          props: { ...el.dataset },
        }
        return h(SeparateOdds, context)
      },
    }).$mount(el)
  })
}

if (betslipEl) {
  new Vue({
    store,
    render: function(h) {
      const context = {
        props: { ...betslipEl.dataset },
      }
      return h(Betslip, context)
    },
  }).$mount(betslipEl)
}
