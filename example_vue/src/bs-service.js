import { getParameterByUrl } from '@/utils/'
const token = getParameterByUrl('token')
const state = getParameterByUrl('state')
const userName = getParameterByUrl('username')
const customerKey = getParameterByUrl('customerKey')
const operator = localStorage.getItem('bs-operator')
let accounts = JSON.parse(localStorage.getItem('bs-accounts')) || []

if (token && userName && customerKey) {
  const bookmaker = {
    token,
    userName,
    customerKey,
    state,
    operator,
  }
  const idx = accounts.findIndex(item => item.operator === operator)
  if (idx === -1) {
    accounts.push(bookmaker)
  } else {
    accounts[idx] = bookmaker
  }
  localStorage.setItem('bs-accounts', JSON.stringify(accounts))
}

if (state) {
  const selectedOperator = accounts.find(item => item.operator === operator)
  selectedOperator.state = state
  localStorage.setItem('bs-accounts', JSON.stringify(accounts))
}
