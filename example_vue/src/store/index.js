import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import auth from './modules/auth/'
import events from './modules/events/'
import selections from './modules/selections/'
import betslip from './modules/betslip/'
import operators from './modules/operators/'
// import createLogger from 'vuex/dist/logger'

Vue.use(Vuex)

const createStore = initialState => {
  return new Vuex.Store({
    state: initialState,
    modules: {
      auth,
      events,
      selections,
      betslip,
      operators,
    },
    strict: process.env.NODE_ENV !== 'production',
    plugins: [
      createPersistedState({
        key: 'bs-storage',
        reducer: persistedState => {
          const stateFilter = { ...persistedState }
          const blackList = ['events', 'selections']
          blackList.forEach(module => {
            delete stateFilter[module]
          })
          return stateFilter
        },
      }),
    ],
  })
}

if (module.hot) {
  module.hot.accept(
    ['./modules/auth/', './modules/events/', './modules/selections/', './modules/betslip/', './modules/operators/'],
    () => {
      const auth = require('./modules/auth').default
      const events = require('./modules/events').default
      const selections = require('./modules/selections').default
      const betslip = require('./modules/betslip').default
      const operators = require('./modules/operators').default
      createStore().hotUpdate({
        modules: {
          auth,
          events,
          selections,
          betslip,
          operators,
        },
      })
    }
  )
}

export default createStore
