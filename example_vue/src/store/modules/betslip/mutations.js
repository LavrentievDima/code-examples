import Vue from 'vue'

const selectBet = (state, bet) => {
  state.bets = [...state.bets, bet]
  state.receipt = null
  state.betsRequest = null
}

const setMultiplesBet = (state, bets) => {
  state.betsMultiples = bets
}

const clearMultiplesBets = state => {
  state.betsMultiples = []
}

const setStake = (state, { id, stake, isValid }) => {
  const selectedBet = [...state.bets, ...state.betsMultiples].find(bet => bet.id === id)
  Vue.set(selectedBet, 'stake', stake)
  Vue.set(selectedBet, 'isValid', isValid)
}

const setBetsRequest = (state, { operator, newBets }) => {
  state.betsRequest = {
    operator_name: operator,
    bets: newBets,
  }
}

const setReturn = (state, { currentBet, win }) => {
  Vue.set(currentBet, 'potReturn', win)
}

const removeBet = (state, id) => {
  state.bets = state.bets.filter(selection => selection.id !== id)
  state.receipt = null
}

const betRequest = state => {
  state.status = 'loading'
  state.error = null
}

const betSuccess = (state, data) => {
  state.status = 'success'
  state.receipt = data
}

const betError = (state, err) => {
  state.status = 'error'
  state.receipt = null
  state.error = err
}

const betReuse = state => {
  state.receipt = null
}

const toggleFrame = (state, payload) => {
  state.showFrame = !state.showFrame
  state.frameContent = payload
}

const resetBetslipState = state => {
  state.bets = []
  state.betsMultiples = []
  state.betsRequest = null
  state.receipt = null
  state.isValid = false
  state.error = null
  state.status = null
  state.showFrame = false
  state.frameContent = 'auth'
  state.totalStake = 0
  state.totalReturn = 0
}

const setTotalStake = (state, payload) => {
  if (payload.length > 0) {
    let initialStake = 0
    const totalStake = payload.reduce((accumulator, currentValue) => {
      return accumulator + currentValue
    }, initialStake)
    state.totalStake = totalStake
  }
}

const setTotalReturn = (state, payload) => {
  if (payload.length > 0) {
    let initialReturn = 0
    const totalReturn = payload.reduce((accumulator, currentValue) => {
      return accumulator + currentValue
    }, initialReturn)
    state.totalReturn = totalReturn
  }
}

const removeApiErrors = (state, payload) => {
  Object.keys(payload)
    .filter(key => !!payload[key].error)
    .map(el => {
      payload[el].error = null
    })
}

const priceChanged = (state, payload) => {
  state.bets.forEach(bet => {
    payload.forEach(i => {
      if (bet.id === i.selection_key) {
        Vue.set(bet.selections[0], 'numerator', parseInt(i.numerator))
        Vue.set(bet.selections[0], 'denominator', parseInt(i.denominator))
      }
    })
  })
}

const checkValid = (state, payload) => {
  state.isValid = payload
}

const clearLocalStorage = () => {
  localStorage.removeItem('bs-accounts')
  localStorage.removeItem('bs-operator')
  localStorage.removeItem('bs-storage')
}

const setNewVersion = (state, { config, version }) => {
  Vue.set(config, 'version', version)
}

export default {
  selectBet,
  setStake,
  setBetsRequest,
  setReturn,
  removeBet,
  betRequest,
  betSuccess,
  betError,
  betReuse,
  toggleFrame,
  resetBetslipState,
  setTotalStake,
  setTotalReturn,
  removeApiErrors,
  priceChanged,
  checkValid,
  clearLocalStorage,
  setNewVersion,
  setMultiplesBet,
  clearMultiplesBets,
}
