import api from '@/services/api'
import * as betlib from 'betlib/dist/betlib'
import oddslib from 'oddslib'

const selectBet = ({ commit }, selection) => {
  const bet = {
    bet_type: 'SGL',
    each_way: false,
    id: selection.selection_key,
    selections: [selection],
    eventId: selection.event_id,
    typeString: 'single',
    stake: null,
    isValid: false,
    potReturn: {
      totalReturn: 0,
      totalStake: 0,
    },
  }
  commit('selectBet', bet)
}


const removeBet = ({ commit }, payload) => {
  commit('removeBet', payload)
}

const toggleBet = ({ dispatch, getters }, selection) => {
  const isSelected = getters.isActiveBet(selection.selection_key)
  if (isSelected) {
    dispatch('removeBet', selection.selection_key)
  } else {
    dispatch('selectBet', selection)
  }
}

const setStake = ({ commit, dispatch }, payload) => {
  commit('setStake', payload)
  dispatch('setReturn', payload.id)
  dispatch('setTotalStake')
}

const setReturn = ({ commit, state, dispatch }, payload) => {
  const concatBets = [...state.bets, ...state.betsMultiples]
  const currentBet = concatBets.find(bet => bet.id === payload)
  const transformBetSelections = currentBet.selections.map(selection => {
    const decimalOddsTranform = oddslib
      .from('fractional', `${selection.numerator}/${selection.denominator}`)
      .to('decimal')
    return {
      odds: decimalOddsTranform,
      id: selection.event_id,
      market: selection.market_name,
      avaible: true,
    }
  })
  const currentBetSelections = [...transformBetSelections]
  for (let i = 0; i < currentBetSelections.length; i++) {
    let biggestNum = currentBetSelections[i].odds
    let marketName = currentBetSelections[i].market
    for (let j = 0; j < currentBetSelections.length; j++) {
      if (i !== j) {
        if (currentBetSelections[i].id === currentBetSelections[j].id) {
          if (biggestNum <= currentBetSelections[j].odds && marketName === currentBetSelections[j].market) {
            currentBetSelections[i].avaible = false
            biggestNum = currentBetSelections[j].odds
            marketName = currentBetSelections[j].market
          }
        }
      }
    }
  }
  const betLibSelections = currentBetSelections.map(item => {
    return new betlib.WinSelection({
      winOdds: item.odds,
      id: item.id,
      avaible: item.avaible,
    })
  })
  const bet = new betlib.Bet(currentBet.typeString, currentBet.stake, false)
  const potReturns = bet.settle(betLibSelections)
  const win = {
    totalReturn: potReturns.totalReturn(),
    totalStake: potReturns.totalStake(),
  }
  commit('setReturn', { currentBet, win })
  dispatch('setTotalReturn')
}

const setBetsRequest = ({ commit, state, getters }) => {
  const operator = getters.getSelectedOperator
  const filteredValidBets = [...state.bets, ...state.betsMultiples].filter(bet => bet.isValid)
  const newBets = filteredValidBets.map(bet => {
    let newBet = { ...bet }
    const newSelections = newBet.selections.map(selection => {
      const newSelection = {
        price_type: selection.price_type,
        numerator: selection.numerator,
        denominator: selection.denominator,
        selection_key: selection.selection_key,
        meta: selection.meta,
      }
      return newSelection
    })
    newBet.selections = newSelections
    delete newBet.eventId
    delete newBet.id
    delete newBet.combinations
    delete newBet.name
    delete newBet.potReturn
    delete newBet.typeString
    delete newBet.isValid
    return newBet
  })
  commit('setBetsRequest', { operator, newBets })
}

const betReuse = ({ commit }) => {
  commit('betReuse')
}

const toggleFrame = ({ commit, getters }, payload) => {
  const { isAuthenticated } = getters
  if (!payload) {
    payload = 'auth'
    if (isAuthenticated) {
      payload = 'deposit'
    }
  }
  commit('toggleFrame', payload)
}

const setTotalStake = ({ commit, state }) => {
  const stakes = [...state.bets, ...state.betsMultiples].map(bet => bet.potReturn.totalStake)
  commit('setTotalStake', stakes)
}

const setTotalReturn = ({ commit, state }) => {
  const returns = [...state.bets, ...state.betsMultiples].map(bet => bet.potReturn.totalReturn)
  commit('setTotalReturn', returns)
}

const removeApiErrors = ({ commit, rootState }) => {
  commit('removeApiErrors', rootState)
}

const priceChanged = ({ state, commit, dispatch }, payload) => {
  commit('priceChanged', payload)
  state.betsMultiples.forEach(bet => {
    dispatch('setReturn', bet.id)
  })
}

const checkValid = ({ commit, state }) => {
  const getValidBets = [...state.bets.map(item => item.isValid), ...state.betsMultiples.map(item => item.isValid)]
  const isValid = getValidBets.some(item => item)
  commit('checkValid', isValid)
}

const validateRelease = ({ commit, rootState }) => {
  // eslint-disable-next-line no-undef
  const version = BS_VERSION
  const config = rootState.appConfig
  if (config.version !== version) {
    commit('resetAuthState')
    commit('resetBetslipState')
    commit('resetOperatorsState')
    commit('setNewVersion', { config, version })
    commit('clearLocalStorage')
  }
}

export default {
  selectBet,
  removeBet,
  setStake,
  setReturn,
  setBetsRequest,
  betReuse,
  betRequest,
  toggleFrame,
  setTotalStake,
  setTotalReturn,
  toggleBet,
  removeApiErrors,
  priceChanged,
  checkValid,
  validateRelease,
  setMultiplesBet,
}
