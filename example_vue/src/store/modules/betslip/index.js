import getters from './getters'
import mutations from './mutations'
import actions from './actions'

const state = {
  bets: [],
  betsMultiples: [],
  betsRequest: null,
  receipt: null,
  isValid: false,
  error: null,
  status: null,
  showFrame: false,
  frameContent: 'auth',
  totalStake: 0,
  totalReturn: 0,
  defaultMultiples: [
    {
      type: 'DBL',
      value: 2,
      name: 'Doubles',
      typeString: 'double',
    },
    {
      type: 'TBL',
      value: 3,
      name: 'Trebles',
      typeString: 'treble',
    },
    {
      type: 'ACCA',
      value: 4,
      name: 'fold Accumulator',
      typeString: 'accumulator',
    },
  ],
}

export default {
  state,
  getters,
  mutations,
  actions,
}
