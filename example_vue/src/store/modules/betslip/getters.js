import { getCombinations } from '@/utils/index'
import numberToWords from 'number-to-words'

const getStakeById = state => id => {
  const concatBets = [...state.bets, ...state.betsMultiples]
  const currentBet = concatBets.find(bet => bet.id === id)
  if (currentBet !== undefined) {
    return currentBet.stake
  }
}

const isShowFrame = state => state.showFrame

const getApiErrors = (state, getters, rootState) => {
  return Object.keys(rootState)
    .filter(key => !!rootState[key].error)
    .map(el => {
      return rootState[el].error
    })
}

const betsLengthUnique = state => {
  const filteredBetsById = [...new Set(state.bets.map(bet => bet.eventId))].filter(bet => bet !== undefined)
  return filteredBetsById.length
}

const getMultipleCombinations = state => count => {
  return getCombinations(state.bets, count).length
}

const getMultipleData = state => count => {
  const lastElement = { ...state.defaultMultiples[state.defaultMultiples.length - 1] }
  let results = state.defaultMultiples.filter(x => x.value < count + 1 && x.value !== lastElement.value)
  if (count >= lastElement.value) {
    for (let i = lastElement.value; i <= count; i++) {
      let newEl = {
        ...lastElement,
        value: i,
        type: `${lastElement.type}${i}`,
        name: `${numberToWords.toWords(i)}${lastElement.name}`,
        typeString: `${lastElement.typeString}:${i}`,
      }
      results.push(newEl)
    }
  }
  return results
}

const isActiveBet = state => id => {
  return state.bets.map(i => i.id).includes(id)
}

export default {
  betsLengthUnique,
  getStakeById,
  getApiErrors,
  isShowFrame,
  getMultipleCombinations,
  getMultipleData,
  isActiveBet,
}
