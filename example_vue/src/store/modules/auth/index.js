import getters from './getters'
import mutations from './mutations'
import actions from './actions'

const state = {
  status: null,
  error: null,
  accounts: [],
  currency: 'EUR',
}

export default {
  state,
  getters,
  mutations,
  actions,
}
