import Vue from 'vue'

const addAccount = (state, payload) => {
  state.accounts = [...state.accounts, payload]
}

const authLogout = (state, payload) => {
  localStorage.setItem('bs-accounts', JSON.stringify(payload))
  state.accounts = payload
  state.status = ''
  state.error = null
}

const walletRequest = state => {
  state.status = 'loading'
  state.error = null
}

const walletSuccess = (state, { account, balance, currency }) => {
  state.status = 'success'
  Vue.set(account, 'balance', balance)
  Vue.set(account, 'currency', currency)
}

const walletError = (state, err) => {
  state.status = 'error'
  state.error = err
}

const setCurrency = (state, payload) => {
  state.currency = payload
}

const resetAuthState = state => {
  state.status = null
  state.error = null
  state.accounts = []
  state.currency = 'EUR'
}

export default {
  authLogout,
  addAccount,
  walletRequest,
  walletSuccess,
  walletError,
  setCurrency,
  resetAuthState,
}
