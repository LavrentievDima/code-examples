import getSymbolFromCurrency from 'currency-symbol-map'

const getAccount = (state, getters) => state.accounts.find(account => account.operator === getters.getSelectedOperator)

const isAuthenticated = (state, getters) => !!getters.getAccount

const getAuthStatus = state => state.status

const getAuthToken = (state, getters) => {
  if (getters.getAccount) {
    return getters.getAccount.token
  }
}

const getWalletStatus = state => state.status

const getCurrencySymbol = state => getSymbolFromCurrency(state.currency)

export default {
  isAuthenticated,
  getAuthStatus,
  getAuthToken,
  getWalletStatus,
  getCurrencySymbol,
  getAccount,
}
