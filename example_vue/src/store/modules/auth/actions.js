import api from '@/services/api'

const authLogout = ({ commit, dispatch, getters, state }) => {
  const selectedOperator = getters.getSelectedOperator
  const filteredAccounts = state.accounts.filter(account => account.operator !== selectedOperator)
  commit('authLogout', filteredAccounts)
  if (getters.isShowFrame) {
    dispatch('toggleFrame')
  }
}

const addAccount = ({ commit }, payload) => {
  commit('addAccount', payload)
}

const walletRequest = async ({ getters, commit, dispatch, state }) => {
  commit('walletRequest')
  try {
    const operator = getters.getSelectedOperator
    const { data } = await api.post(
      'wallets',
      {
        operator,
      },
      {
        headers: {
          'X-Access-Token': getters.getAuthToken,
        },
      }
    )
    const mainWallet = data.data.items.find(wallet => wallet.name === 'VEGAS')
    if (mainWallet !== undefined) {
      const { balance, currency } = mainWallet
      const account = state.accounts.find(account => account.operator === operator)
      commit('walletSuccess', { account, balance, currency })
      if (state.currency !== currency) {
        commit('setCurrency', currency)
      }
    } else {
      dispatch('authLogout')
    }
  } catch (err) {
    const error = { ...err.response.data, code: err.response.status }
    commit('walletError', err)
    if (error.code === 401) {
      dispatch('authLogout')
    }
  }
}

export default {
  authLogout,
  addAccount,
  walletRequest,
}
